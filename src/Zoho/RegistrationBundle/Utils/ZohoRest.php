<?php

namespace Zoho\RegistrationBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ZohoRest {
    private $url = "";//"https://crm.zoho.com/crm/private/xml/%s/%s";
    private $auth_token = "";//19820499c36e4f0ec1aaa7c57340cfa5";
    private $params = "";
    private $container;

    function __construct($containerInstance, $module, $apiMethod)
    {
        $this->container = $containerInstance;
        $this->url = $this->container->getParameter('zoho_api_url');
        $this->auth_token = $this->container->getParameter('zoho_auth_token');
        //$this->url = sprintf($this->url, $module, $apiMethod);
        $this->url = $this->url . $module . "/" . $apiMethod;
        $this->params = $this->setParameter('authtoken', $this->auth_token, $this->params);
        $this->params = $this->setParameter('scope', 'crmapi', $this->params);
    }

    private function setParameter($key, $value, $parameter)
    {
        if ($parameter === "") {
            $parameter .= $key . '=' . $value;
        } else {
            $parameter .= '&' . $key . '=' . $value;
        }

        return $parameter;
    }

    private function sendCurlRequest()
    {
        try {
            /* initialize curl handle */
            $ch = curl_init();
            /* set url to send post request */
            curl_setopt($ch, CURLOPT_URL, $this->url);
            /* allow redirects */
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            /* return a response into a variable */
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* times out after 30s */
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            /* set POST method */
            curl_setopt($ch, CURLOPT_POST, 1);
            /* add POST fields parameters */
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->params);
            /* execute the cURL */
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        } catch (Exception $exception) {
            echo 'Exception Message: ' . $exception->getMessage() . '<br/>';
            echo 'Exception Trace: ' . $exception->getTraceAsString();
        }
    }

    private function checkResponse($response) {
        $html = new \DOMDocument();
        $html->loadXML($response);

        if ($err = $html->getElementsByTagName('error')->item(0)) {
            return false;
        }

        return true;
    }

    public function insertRecords($xml_representation)
    {
        $this->params = $this->setParameter('xmlData', $xml_representation, $this->params);
        $response = $this->sendCurlRequest();
        return $this->checkResponse($response);
    }
}

?>