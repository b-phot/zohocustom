<?php
namespace Zoho\RegistrationBundle\Form\Entity;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LeadForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $default_option = "Choose an Option";

        $builder->add('first_name')
            ->add('last_name')
            ->add('company')
            ->add('title', 'text', array('label' => 'Job Title'))
            ->add('id', 'repeated', array(
                'invalid_message' => 'The E-Mail fields must match',
                'first_options' => array('label' => 'E-Mail Address'),
                'second_options' => array('label' => 'Confirm E-Mail Address'),
                ))
            // ->add('id', 'email', array('label' => 'E-Mail Address'))
            // ->add('email_confirm', 'text', array('mapped' => false, 'label' => 'Confirm E-Mail Address'))
            ->add('phone')
            ->add('country')
            ->add('company_type', 'choice', array('label' => 'Company Size',
                'choices' => array(
                    '' => $default_option,
                    "Small-to-Medium Enterprise (SME)" => "Small-to-Medium Enterprise (SME)",
                    "Large-Scale Company (LSC)" => "Large-Scale Company (LSC)",
                    "Other" => "Other",
                    /*'Industry - SME' => 'Industry - SME',
                    'Industry - LSC' => 'Industry - LSC'*/
                    )
                ))
            ->add('industry', 'choice', array('label' => 'Industry Sector',
                'choices' => array(
                    '' => $default_option,
                    'Photonics - general' => 'Photonics - general',
                    'Aerospace'=>'Aerospace',
                    'Automotive'=>'Automotive',
                    'Building & Construction' => 'Building & Construction',
                    'Communications' => 'Communications',
                    'Energy'=>'Energy',
                    'Entertainment' => 'Entertainment',
                    'Food'=>'Food',
                    'Medical/Life Sciences'=>'Medical/Life Sciences',
                    'Lighting & Displays'=>'Lighting & Displays',
                    'Plastics'=>'Plastics',
                    'Production Technology'=>'Production Technology',
                    'Textiles'=>'Textiles',
                    'Other'=>'Other'
                    )
                ))
            ->add('lead_source', 'choice', array('label' => 'How did you hear about ACTPHAST?',
                'choices' => array(
                    '' => $default_option,
                    'Advertising' => 'Advertising',
                    'Mailing' => 'Mailing',
                    'Press release' => 'Press release',
                    'Website' => 'Website',
                    'Event' => 'Event',
                    'Direct Contact' => 'Direct Contact',
                    'External Referral' => 'External Referral',
                    'Other' => 'Other'
                    )
                ))
            ->add('english_speaker', 'choice', array('label' => 'Are you happy to conduct discussions in English?',
                'choices' => array(
                    '' => $default_option,
                    'Yes' => 'Yes',
                    'No' => 'No'
                    )
                ))
            ->add('description', 'textarea', array('label' => 'Can you provide a brief description of the innovation project that you would like to discuss with ACTPHAST?(max 300 words)'))
            ->add('lead_owner', 'choice', array(
                'choices' => array(
                    'Peter Doyle' => 'Peter Doyle'
                    )
                ))
            ->add('lead_outreach_partner', 'choice', array(
                'choices' => array(
                    'VUB' => 'VUB',
                    'CNIT' => 'CNIT',
                    'CNRS' => 'CNRS',
                    'HHI' => 'HHI',
                    'ICCS' => 'ICCS',
                    'ICFO' => 'ICFO',
                    'KIT' => 'KIT',
                    'ORC' => 'ORC',
                    'Tyndall-UCC' => 'Tyndall-UCC',
                    'UEF' => 'UEF',
                    'WUT' => 'WUT'
                    )
                ))
            ->add('rating', 'choice', array(
                'choices' => array(
                    'Non-qualified lead' => 'Non-qualified lead',
                    'Qualified lead' => 'Qualified lead'
                    )
                ))
            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'lead';
    }
}
?>