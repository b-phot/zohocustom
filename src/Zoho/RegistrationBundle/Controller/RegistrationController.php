<?php
namespace Zoho\RegistrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zoho\RegistrationBundle\Entity\Lead;
use Zoho\RegistrationBundle\Form\Entity\LeadForm;
use Zoho\RegistrationBundle\Utils\ZohoRest;


class RegistrationController extends Controller
{
    public function indexAction(Request $request){
        $lead = new Lead();
        $form = $this->createForm(new LeadForm(), $lead);
        $form->handleRequest($request);

        if ($form->isValid()){
            $dm = $this->getDoctrine()->getManager();
            $lead->setSynced(0);
            $fields_map = $this->container->getParameter('zoho_fields_map');
            $xml_representation = $lead->getXmlRepresentationZoho($fields_map);
            $rest_client = new ZohoRest($this->container, "Leads", "insertRecords");
            if ($rest_client->insertRecords($xml_representation)){
                $lead->setSynced(1);
            }
            $dm->persist($lead);
            $dm->flush();
            return $this->redirect($this->generateUrl(
                    'lead_success',
                    array('id' => $lead->getId())
                    )
            );
        }

        return $this->render('ZohoRegistrationBundle:Registration:index.html.twig',
            array('form' => $form->createView(),
                ));
    }

    public function successAction(Request $request) {
        $id = $request->query->get('id');
        $lead = $this->getDoctrine()
            ->getRepository('ZohoRegistrationBundle:Lead')
            ->find($id);
        if (!$lead){
            throw $this->createNotFoundException('No LEAD found with id '.$id);
        }
        return $this->render('ZohoRegistrationBundle:Registration:index.html.twig',
            array(
                'name' => $lead->getFirstName(),
                'synced' => $lead->getSynced(),
                ));
    }
}
?>