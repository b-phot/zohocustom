<?php

namespace Zoho\RegistrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lead
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Lead
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=100)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=100)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="company_type", type="string", length=20)
     */
    private $companyType;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_outreach_partner", type="string", length=20)
     */
    private $leadOutreachPartner;

    /**
     * @var string
     *
     * @ORM\Column(name="industry", type="string", length=50)
     */
    private $industry;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_owner", type="string", length=100)
     */
    private $leadOwner;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_source", type="string", length=100)
     */
    private $leadSource;

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="string", length=50)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="english_speaker", type="string", length=3)
     */
    private $englishSpeaker;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="synced", type="boolean")
     */
    private $synced;

    /**
    * Set Id
    *
    * @param string $id
    * @return Lead
    */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Lead
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Lead
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Lead
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companyType
     *
     * @param string $companyType
     * @return Lead
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get companyType
     *
     * @return string
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * Set leadOutreachPartner
     *
     * @param string $leadOutreachPartner
     * @return Lead
     */
    public function setLeadOutreachPartner($leadOutreachPartner)
    {
        $this->leadOutreachPartner = $leadOutreachPartner;

        return $this;
    }

    /**
     * Get leadOutreachPartner
     *
     * @return string
     */
    public function getLeadOutreachPartner()
    {
        return $this->leadOutreachPartner;
    }

    /**
     * Set industry
     *
     * @param string $industry
     * @return Lead
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return string
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set leadOwner
     *
     * @param string $leadOwner
     * @return Lead
     */
    public function setLeadOwner($leadOwner)
    {
        $this->leadOwner = $leadOwner;

        return $this;
    }

    /**
     * Get leadOwner
     *
     * @return string
     */
    public function getLeadOwner()
    {
        return $this->leadOwner;
    }


    /**
     * Gets the value of leadSource.
     *
     * @return string
     */
    public function getLeadSource()
    {
        return $this->leadSource;
    }

    /**
     * Sets the value of leadSource.
     *
     * @param string $leadSource the lead source
     *
     * @return self
     */
    public function setLeadSource($leadSource)
    {
        $this->leadSource = $leadSource;

        return $this;
    }

    /**
     * Set rating
     *
     * @param string $rating
     * @return Lead
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set englishSpeaker
     *
     * @param boolean $englishSpeaker
     * @return Lead
     */
    public function setEnglishSpeaker($englishSpeaker)
    {
        $this->englishSpeaker = $englishSpeaker;

        return $this;
    }


    /**
     * Get englishSpeaker
     *
     * @return boolean
     */
    public function getEnglishSpeaker()
    {
        return $this->englishSpeaker;
    }

    /**
     * Get synced
     *
     * @return boolean
     */
    public function getSynced()
    {
        return $this->synced;
    }

    /**
     * Set synced
     *
     * @param boolean $synced
     * @return Lead
     */
    public function setSynced($synced)
    {
        $this->synced = $synced;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Lead
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the value of country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }


    /**
     * Sets the value of country.
     *
     * @param string $country
     * @return Lead
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Gets the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the value of title.
     *
     * @param string $title the title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

     /**
     * Gets the value of phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the value of phone.
     *
     * @param string $phone the phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /** XML **/
    public function getXmlRepresentationZoho($fields_map)
    {
        $object_xml = new \SimpleXMLElement("<Leads/>");
        $row = $object_xml->addChild("row");
        $row->addAttribute("no", "1");

        foreach(get_object_vars($this) as $key => $value){
            if (array_key_exists($key, $fields_map)){
                $fl = $row->addChild("FL", $value);
                $fl->addAttribute("val", $fields_map[$key]);
            }
        }
        $dom = dom_import_simplexml($object_xml);

        return $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);
    }


}
